/*
Title:  Practical Programming Test
Name:   Victoria Laubli 10000969
Date:    14th June 2018
Purpose: To write an application to demonstrate awareness of procedural and object oriented programming.
*/

alert("Hi welcome");

//Declare variables
let VanID = ""
let Capacity = ""
let License = ""
let CostPerDay = ""
let Insurance = ""

VanID = prompt("EZ Van Hire: Enter Van Details Van ID")

Capacity = prompt("EZ Van Hire: Enter Van Details Load Capacity (M3)")

License  = prompt("EZ Van Hire: Enter Van Details License Type")

CostPerDay = prompt("EZ Van Hire: Enter Van Details Hire Cost Per Day: $")

Insurance = prompt("EZ Van Hire: Enter Van Details Insurance Per Day: $")


class Van {

    get VanID() {                       //get method for Van ID
        return this._VanID      
    }
 
    set VanID(value) {                  //set method for Van ID
        this._VanID = value
    }
 
   
    get Capacity() {                      //get method for Capacity
        return this._Capacity
    }
 
    set Capacity(value) {                 //set method for Capacity
        this._Capacity = value
    }
 
 
    get License() {                         //get method for License
        return this._License
    }
 
    set License(value) {                    //set method for License
        this._License = value
    }
 
 
    get CostPerDay() {                        //get method for the Cost Per Day
        return this._CostPerDay
    }
 
    set CostPerDay(value) {                   //set method for the Cost Per Day
        this._CostPerDay = value
    }
 
    get Insurance() {                        //get method for the Insurance
        return this._Insurance
    }
 
    set Insurance(value) {                   //set method for the Insurance
        this._Insurance = value
    }
   
      
 
 
    //Constructor for VanID, Capacity, License, CostPerDay and Insurance
    constructor(VanID, Capacity , License, CostPerDay, Insurance) {
        this.VanID       = VanID                                    //set VanID
        this.Capacity    = Capacity                                 //set Capacity
        this.License     = License                                  //set tv License
        this.CostPerDay  = CostPerDay                               //set CostPerDay
        this.Insurance   =Insurance                                 //set Insurance
    }
 

    //Method definition - "Van Details()"
   //Returns a string comprising of the all the Van details
   VanDetails(){
    return  `Van DETAILS\n` +
            `---------------\n` +
            `Van ID: ${this._VanID}\n` +
            `Load Capacity: ${this._Capacity}\n` +
            `License Type: ${this._License}\n`+
            `Hire Cost Per Day: ${this._CostPerDay} ${this._CostPerDay}\n` +
            `Insurance Per Day: ${this._Insurance}\n`
   
}
    TotalHireCost(){
        return (this.CostPerDay - this.Insurance * this.CostPerDay / 100)
    }
}


//MAIN PROGRAM

console.log()

//Instantiate (create) a new object "VanX" using the "Van" class as a "template/blueprint"
//Pass arguments to the class
let VanX = new Van( VanID, Capacity, License, CostPerDay, Insurance )

console.log()


//console.log(VanX)


console.log()


//Use the "VanXDetails()" Method to display the required list of Van details
console.log(`${VanX.VanDetails()}`)
console.log(`Total Hire Cost: ${VanX.TotalHireCost()}`)

console.log()
